<?php

function ctools_languages_drush_command() {
  $items = array();

  $items['menu-list'] = array(
    'description' => "Outputs a list of all menus.",
    'examples' => array(
      'drush menu-list',
    ),
  );

  $items['locales-menuitems'] = array(
    'description' => "Writes menu items to the locales_source table for exporting.",
    'options' => array(
      'menu' => 'A specific menu to write to the locales_source table. Optional.',
    ),
    'examples' => array(
      'drush locales-menuitems',
      'drush locales-menuitems --menu=menu-custom-menu',
    ),
    'aliases' => array('lmi'),
  );

  $items['locales-rm-menuitems'] = array(
    'description' => "Removes all menu items from locales_source and all their translations from locales_target",
    'options' => array(
      'menu' => 'A specific menu for which to remove entries.',
    ),
    'examples' => array(
      'drush locales-rm-menuitems',
      'drush locales-rm-menuitems --menu=menu-custom-menu',
    ),
    'aliases' => array('lmirm'),
  );

  return $items;
}

function drush_ctools_languages_menu_list() {
  drush_print_r(menu_get_menus());
}

function drush_ctools_languages_locales_menuitems() {
  $menus = menu_get_menus();
  if ($menu = drush_get_option('menu')) {
    if (isset($menus[$menu])) {
      $menus = array(
        $menu => $menus[$menu],
      );
    }
  }
  foreach ($menus as $menu_name => $menu_title) {
    $query = db_select('menu_links', 'ml', array('fetch' => PDO::FETCH_ASSOC));
    $query->fields('ml');
    $query->condition('ml.menu_name', $menu_name);
    foreach ($query->execute() as $item) {
      $source_query = db_select('locales_source', 'ls')
        ->fields('ls', array('lid'))
        ->condition('location', 'menu.item.' . $item['mlid'] . '.link_title');
      $result = $source_query->execute()->fetchAssoc();
      if (!$result) {
        $source = new stdClass();
        $source->location = 'menu.item.' . $item['mlid'] . '.link_title';
        $source->textgroup = 'menu';
        $source->source = $item['link_title'];
        $source->context = 'menu.item.link_title';
        $source->version = 1;
        drupal_write_record('locales_source', $source);
      }
    }
  }
}

function drush_ctools_languages_locales_rm_menuitems() {
  $menus = menu_get_menus();
  if ($menu = drush_get_option('menu')) {
    if (isset($menus[$menu])) {
      $menus = array(
        $menu => $menus[$menu],
      );
    }
  }
  foreach ($menus as $menu_name => $menu_title) {
    $query = db_select('menu_links', 'ml', array('fetch' => PDO::FETCH_ASSOC));
    $query->fields('ml');
    $query->condition('ml.menu_name', $menu_name);
    foreach ($query->execute() as $item) {
      $source_query = db_select('locales_source', 'ls')
        ->fields('ls', array('lid'))
        ->condition('location', 'menu.item.' . $item['mlid'] . '.link_title');
      $result = $source_query->execute()->fetchAssoc();
      if ($result) {
        db_delete('locales_source')
          ->condition('lid', $result['lid'])
          ->execute();
        db_delete('locales_target')
          ->condition('lid', $result['lid'])
          ->execute();
      }
    }
  }
}